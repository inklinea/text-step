#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Text Step - An Inkscape 1.1+ extension
# Just some silly text stepping using tspans.
# Appears Under Extensions>Text>Text Step

import inkex
from inkex import TextElement, Tspan, TextPath, Boolean

from random import shuffle


def make_tspan_element(self, sample_text, font_size, phrase_spacing):
    tspan = Tspan()
    tspan.text = sample_text
    tspan.style['font-size'] = font_size
    if phrase_spacing > 0:
        tspan.set('dx', phrase_spacing)

    return tspan


def make_text_element(self):
    text_element = TextElement()
    text_element.set('x', '10')
    text_element.set('y', '10')

    return text_element


def make_text_from_phrase(self):
    sample_text = self.options.user_text_string

    text_element = make_text_element(self)

    # Important - or whitespace is ignored !
    text_element.set('xml:space', 'preserve')

    font_size_start = self.options.font_size_start_float
    font_size_end = self.options.font_size_end_float

    steps = self.options.steps_int

    step_size = (font_size_end - font_size_start) / (steps - 1)

    tspan_size_list = []

    # Make the start tspan
    tspan_size_list.append(font_size_start)
    # loop through the middle
    current_font_size = font_size_start
    for count in range(2, steps):
        current_font_size = current_font_size + step_size
        tspan_size_list.append(current_font_size)
    # Make the end tspan
    tspan_size_list.append(font_size_end)

    # inkex.errormsg(tspan_size_list)

    # Set the direction
    if self.options.size_order_radio == 'reverse':
        tspan_size_list.reverse()
    elif self.options.size_order_radio == 'random':
        shuffle(tspan_size_list)
    else:
        pass

    first_tspan = True

    # Should we ignore the first item phrase_spacing
    # As this results as an x0 offset with text on path
    for font_size_value in tspan_size_list:

        if first_tspan:
            if not self.options.first_phrase_offset_cb:
                tspan = make_tspan_element(self, sample_text, font_size_value, 0)
            else:
                tspan = make_tspan_element(self, sample_text, font_size_value, self.options.first_phrase_offset_float)
            first_tspan = False
            text_element.append(tspan)
            continue

        if not self.options.phrase_spacing_cb:
            tspan = make_tspan_element(self, sample_text, font_size_value, 0)
        else:
            tspan = make_tspan_element(self, sample_text, font_size_value, self.options.phrase_spacing_float)

        text_element.append(tspan)

    return text_element


def put_text_on_Path(self, stepped_phrase_element, path):
    text_path = TextPath()
    text_path.href = path.get_id()
    text_path.append(stepped_phrase_element)

    return text_path


class TextStep(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--text_step_notebook", type=str, dest="polygon_side_notebook", default=0)

        pars.add_argument("--font_size_start_float", type=float, dest="font_size_start_float", default=5)
        pars.add_argument("--font_size_end_float", type=float, dest="font_size_end_float", default=25)

        pars.add_argument("--steps_int", type=int, dest="steps_int", default=5)

        pars.add_argument("--size_order_radio", type=str, dest="size_order_radio", default='forwards')

        pars.add_argument("--reverse_path_cb", type=Boolean, dest="reverse_path_cb", default=False)

        pars.add_argument("--user_text_string", type=str, dest="user_text_string", default='Inkscape is cool !')

        pars.add_argument("--phrase_spacing_cb", type=Boolean, dest="phrase_spacing_cb", default=False)

        pars.add_argument("--phrase_spacing_float", type=float, dest="phrase_spacing_float", default=0)

        pars.add_argument("--first_phrase_offset_cb", type=Boolean, dest="first_phrase_offset_cb", default=False)

        pars.add_argument("--first_phrase_offset_float", type=float, dest="first_phrase_offset_float", default=0)

    def effect(self):

        selection_list = self.svg.selected
        if not 1 < len(selection_list) < 3:
            inkex.errormsg('please select 2 objects')
            return

        tag_list = []
        for item in selection_list:
            tag_list.append(item.TAG)

        if 'text' not in tag_list or 'path' not in tag_list:
            inkex.errormsg('At least one path and one text element required')
            return

        for item in selection_list:
            if item.TAG == 'path':
                path = item
            if item.TAG == 'text':
                text = item
                text_style = text.style

        if self.options.reverse_path_cb:
            path.path = path.path.reverse()

        stepped_phrase_element = make_text_from_phrase(self)

        self.svg.get_current_layer().append(stepped_phrase_element)

        text_path_element = put_text_on_Path(self, stepped_phrase_element, path)

        text_path_text_wrapper = TextElement()

        text_path_text_wrapper.append(text_path_element)

        text_path_text_wrapper.style = text_style

        self.svg.get_current_layer().append(text_path_text_wrapper)


if __name__ == '__main__':
    TextStep().run()
